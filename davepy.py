"""
    Analysis, visualization and exploration of data (davepy) is a library built for python in order to facilitate the manipulation, application of reconstruction methods and observation and export of large amounts of data using libraries such as pandas and numpy.
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import collections
from typing import NewType
from sklearn.linear_model import LinearRegression
import sklearn
####
# I hope to change matplotlib for another dinamic visualization tool
####

#Some usefull variables
colors = ["#274d4c","#e15d5f","#2f9595","#e79094","#ef7d7f"]
sklearnList = ['LinearRegression']


#Objetives:
#   *Read a large set of data
#   *Create a solid data strucuture
#   *Create tools that can be used to explorer data
#   *Create tools that can be used to transform data

#ToDo:
#   *Some methods has no exception conditions
#   *It is functional
#   *Try it with some IA method

##Functions

####Funciones Rodrigo
def generateRandom(cummDist: list) -> int:
    '''
    Genera una variable aleatoria entre [1,len(cummDist)]
    
    Parameters:
    -----------
    
    cummDist: list
        Valid Cummulative Distribution
        
    Returns:
    -------
        
    ts: int
        random integer between [1,len(cummDist)]
        
    >>> np.add(1, 2)
    3
    
    '''
    s = np.random.uniform()
    treshold = s>np.array(cummDist)
    ts = np.sum(treshold)
    return ts

def simulateFailures(n: int, PD_1: list, PD_2: list) -> list:
    '''
    Esta funcion simula fallas en la captura de datos.
    
    
    Parameters:
    -----------
    
    n : int
        Length of the time series
        
    PD_1: list
        Cummulative distribution for failures (holes).
        
    PD_2: list
        Cummulative distribution for continuum data.
    
    
    Returns:
    -------
    
    h : list
        sequence of simmulated failures
        
    
    Example:
    --------

    >>> n = 5
    >>> f1 = [5,5]
    >>> f2 = [5,5]
    >>> H = simulateFailures(n,f1,f2)
    >>> H
    [0.0, 0.0, 1.0, 0.0, 1.0]
    
    
    
    '''
    # Funcion sin nombre de distribucion acumulada normalizada 
    
    f = lambda x:  list(np.cumsum(x/np.sum(x)))
    
    #Generacion de acumulados para cada distribucion
    
    PD_1 = f(PD_1)
    PD_2 = f(PD_2)
    h = list()
    
    while len(h) < n:
        
        # generamos secuencia de datos contiguos (no fallas), de tamaño aleatorio
        
        ts = generateRandom(PD_1)
        ones = np.ones(ts)
        h.extend(ones)
        
        if len(h) < n:
            
            # generamos secuencia de datos faltantes (fallas), de tamaño aleatorio
            
            ts = generateRandom(PD_2)
            zeros = np.zeros(ts)
            h.extend(zeros)
    
    return h[:n]

def mask(h:int,tau:int,m:int)->list:
    """Devuelve una lista binaria (0,1) siguiendo una frecuencia tau para una cantidad m de elementos verdaderos.
    
    Devuelve una lista binaria, de ceros y unos, en el centro del arreglo se coloca una lista de ceros de tamaño h y a los extremos del vector se coloca una lista binaria de tamaño n, que cumple la relación

        n = 2(m) - 1

    La lista devuelta es de tamaño 2n + h
    
    Parameters
    ----------
    h : int
        Cantidad de datos centrales
    tau : int
        -----------------------
    m : int
        -----------------------
    
    Returns
    -------
    list
        Lista binaria de tamaño 2n + h
    """       
    i = j = 0                       #Contadores
    tmp = list()                    #Lista auxiliar
    while(j != m):                  #Verifica que la cantidad de altos contados sea diferente al requerido
        if i%tau == 0:              #Cada múltiplo de tau
            tmp.append(1)           #Añade un alto
            j+=1                    #Cuenta la cantidad de altos
        else:
            tmp.append(0)           #Añade un bajo
        i+=1                        #Avanza al infinito
    vH = [0 for k in range(h)]      #Construye el vector central
    return([tmp+vH+tmp,len(tmp)])



##Object
class BadRequest(Exception):       
    pass

class RestoreData(object):

    def __init__(self,data:list,date:list,name:str='Default',test_size:float=0.6):
        """
           Initialization fuctión, from the list input create a DataFrame object if data and date has the same len.

        Parameters
        ----------
        data : list
            Data is a list of floats, this data set must has NAN places
        date : list
            The date contains the moment the data was recorded. It must be entered in a date format such as %d /%m /%y
        name : str, optional
            At the end of the manipulation the final document it'll have this name, by default Default
        svector : list, optional
            Contains the percentage of each train set, by default [0.6,0.2,0.2]
        """        
        self.__checkdata__(data,date)
        saux = (1-test_size)/2
        self.df = self.__createdf__(data,date,[test_size,saux,saux])
        self.name = name
        #self.size = [self.df['data'].isna().sum(),
        #len(data) - self.df['data'].isna().sum()]
        

    def __createdf__(self,data:list,date:list,svector:list):
        """
           Auxiliar function that create a dataframe

        Parameters
        ----------
        data : list
            Data is a list of floats, this data set must has NAN places
        date : list
            The date contains the moment the data was recorded. It must be entered in a date format such as %d /%m /%y
        svector : list
            Contains the percentage of each train set
        """        
        aux = [round(i*len(data))
                    for i in svector]
        if sum(aux) != len(data):
            aux[-1] += 1
        dicc = {'date':pd.to_datetime(date),
                    'data':map(float,data),
                     'setid':np.repeat(['E','P','V'],aux)[:len(data)]}
        return(pd.DataFrame(data=dicc))

    def __checkdata__(self,data:list,date:list) -> object:
        """
            Data and Date shoud had the same len, that is the reason for checdata function. Check if data and date has the same len, if not return a BadRequest object

        Parameters
        ----------
        data : list
            [Data list input]
        date : list
            [Date list input]

        Returns
        -------
        object
            Return a BadRequest object if the len are not the same, otherwise return a boolean True.
        """ 
        if (type(data) == list) and (type(date) == list):
            if len(data) == len(date):
                return(True)
        raise BadRequest("Please check out that data input has the same len or ther're list")
        

    def summary(self,colname:str='data'):
        """Generate descriptive statistics.

        Descriptive statics and some usefull information about colname data in the dataframe

        Parameters
        ----------
        colname : str, optional
            A name of the data that will be describe, by default 'data'
        """        
        print("---------------------------------")
        print("--------DESCRIBE FUNCTION--------")
        print("")
        print("Object name: {}".format(self.name))
        print("Cols size: {}".format(self.df.shape[1]))
        print("Rows size: {}".format(self.df.shape[0]))
        print("       Oldest date --- Current date")
        print(self.df['date'].min(),'---',self.df['date'].max())
        print("NaN count: {}".format(
           self.df.shape[0] - self.df[colname].count()))
        print("Max: ",self.df[colname].max())
        print("Min: ",self.df[colname].min())
        print("Mode: ",self.df[colname].mode().iloc[0])
        print("Mean (skipna): ",self.df[colname].mean(skipna=True))
        print("")
        print("---------------------------------")
    
    def plot_pie(self,ax,colname:str='data',save:bool=False):
        """Plot a pie chart

        From colname plot the difference between NaN and data

        Parameters
        ----------
        colname : str, optional
            Name of col to read, by default 'data'
        ax : matplotlib, optional
            Position to insert the figure, by default None
        """        
        labels = 'Data','NaN'
        x = self.df[colname].count()/self.df[colname].shape[0]
        y = (self.df[colname].shape[0] - self.df[colname].count())/self.df[colname].shape[0]
        sizes = [x,y]
        explode = (0.2, 0)  # only "explode" the 2nd slice (i.e. 'Hogs')

        ax.pie(sizes, explode=explode, labels=labels, autopct='%1.1f%%',
                shadow=True, startangle=90, colors=colors)
        ax.set_title("Relación de datos")
        ax.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
        if save: plt.savefig("prueba.png")
        



    def plot_bar(self,ax,data:dict,title:str='data',xlabel='Tamaño',ylabel='Frecuencia',nbins = 50,color:int=0,save:bool=False):
        """Plot a bar chart

        Parameters
        ----------
        ax : matplotlib.Axes
            Location where char was deployd
        data : dict
            Data that was deployd
        title : str, optional
            Title of chart, by default 'data'
        xlabel : str, optional
            Set the name of x axis, by default 'Tamaño'
        ylabel : str, optional
            Set the name of y axis, by default 'Frecuencia'
        nbins : int, optional
            Number of bins in the bar char, by default 50
        save : bool, optional
            If True then save the picture, by default False
        """        
        od = collections.OrderedDict(sorted(data.items()))
        key = []
        val = []
        for k, v in od.items():
            key.append(str(k))
            val.append(v)
            
        if len(key) < 50:
            nbins = len(key)

    
        ax.bar(key[:nbins],val[:nbins],color=colors[color])
        ax.set_title("Histograma de {}".format(title))
        ax.set_xlabel(xlabel)
        ax.grid()
        ax.set_ylabel(ylabel)
        #ax.set_xticklabels(key[:nbins], rotation=45)
        if save: plt.savefig("prueba.png")

    def plot_dot(self,ax,X:str='data',title:str='data',xlabel='Tiempo',ylabel='Magnitud',data_por=1,color:int=3,save:bool=False):
        """Plot a bar chart

        Parameters
        ----------
        ax : matplotlib.Axes
            Location where char was deployd
        data : dict
            Data that was deployd
        title : str, optional
            Title of chart, by default 'data'
        xlabel : str, optional
            Set the name of x axis, by default 'Tamaño'
        ylabel : str, optional
            Set the name of y axis, by default 'Frecuencia'
        nbins : int, optional
            Number of bins in the bar char, by default 50
        save : bool, optional
            If True then save the picture, by default False
        """        
        #X = self.df[X].dropna()
        X = self.df[X]
        n = len(X)
        if n < 50:
            n = int(len(X))
        else:
            if data_por == 1:
                n = 50
            else:
                n = int(round(len(X)*data_por))

        ax.plot(X[:n],'-o',color=colors[color])
        ax.set_title("Gráfica de {}".format(title))
        ax.set_xlabel(xlabel)
        ax.grid()
        ax.set_ylabel(ylabel)
        #ax.set_xticklabels(key[:nbins], rotation=45)
        if save: plt.savefig("prueba.png")

    def counter(self,colname:str='data',by:str='size',period:str='Y'):
        """A Counter is a dict for counting iterable objects

        Parameters
        ----------
        colname : str, optional
            Name of the col to count, by default 'data'
        by : str, optional
            Set the parametrer to count, by default 'size'
        period : str, optional
            If the parametret it's time, can select the period, by default 'year'
        """        
        if by == 'size':
            ##¿Se puede optimizar esta condicional?
            l = self.df[colname].isna()
            i = j = 0
            dicj = dict() #NaN counter
            dici = dict() #dt counter
            for item in l:
                if item:
                    if j > 0:
                        try:
                            dicj[j] += 1
                        except:
                            dicj[j] = 1
                        j = 0
                    i +=1

                else:
                    if i > 0:
                        try:
                            dici[i] += 1
                        except:
                            dici[i] = 1
                        i = 0
                    j += 1
            
            if i > 0:
                try:
                    dici[i] += 1
                except:
                    dici[i] = 1
            if j > 0:
                try:
                    dicj[j] += 1
                except:
                    dicj[j] = 1
        elif by == 'time':
            aux = self.df.groupby(pd.Grouper(key='date',freq=period),dropna=True).agg('count')
            keys_l = aux.index.tolist()
            data = aux[colname].tolist()
            nan = [abs(data[i] - aux['setid'].tolist()[i]) for 
                i in range(len(data))]
            dici = dict(zip(keys_l,data))
            dicj = dict(zip(keys_l,nan))
            
        else:
            dici = {}
            dicj = {}
        
        return(dici,dicj)
        
            
    def build(self,func:object,merge:bool=False, test=False,name:str='',h:int = 1,**kwargs) -> list:
        """Build data from a function 

        Parameters
        ----------
        func : object
            The methos that'll be used to build lost data
        merge : bool, optional
            If true then will have a merge beetwen original data and new data, otherwise only new data become new col, by default False
        test : bool, optional
            The input can be a tupple of numpy random function or a tupple of random values list from this we create randoms nan values, otherwise the nan will be from original data, by default False
        name : str, optional
            Name of the new col, by default ''
        h : int, optional
            Size of continuos nan, by default 1*


        Returns
        -------
        list
            Return the new data, alse create a new col.
        """        
        if func in sklearnList:
            if test is False:
                index = self.getIndex('data','nan')
            else:
                index = self.getFakeIndex('data')
        else:
            raise Exception('Not in accepted methods')
        

    def describe(self,colname:str='data',save:bool = False):
        """A faast description of data set

        Parameters
        ----------
        colname : str, optional
            Data to read, by default 'data'
        save : bool, optional
            if images will be save, by default False
        """        
        self.summary(colname)
        print("-----------PLOTS-------------")
        figsize = (12,8)
        title="Resumen de los datos obtenidos en {} de los datos {}".format(colname,self.name)
        #sharex=True,
        fig,ax = plt.subplots(2,3,figsize=figsize, constrained_layout=True)
        fig.suptitle("\n".join([title]), x=0.2)
        fig.tight_layout()
        fig.patch.set_visible(False)
        
        ax[0,0].axis('off')
        ax[0,2].axis('off')
        self.plot_pie(ax[0,1],colname,save)
        nan, dt = self.counter(colname)
        #ax[1,1].axis('off')
        self.plot_bar(ax[1,0],dt)
        self.plot_dot(ax[1,1],colname)
        self.plot_bar(ax[1,2],nan,title='NaN',color=1)
        plt.show()


    def getFakeIndex(self,colname:str='data',dist:list=[],h=None,**kwargs)->dict:
        """Return a dict of the initial position of a secuens of nan type, if and oly if in the colname data are not a nan in these position

        Parameters
        ----------
        colname : str, optional
            Name of the column to serch into, by default 'data'

        Returns
        -------
        dict
            A dict where keys are the size of continuos data and values are the position of those data secuens
        """        
        try:
            aux = self.getIndex(colname=colname)
            #sort_idx = sorted(aux.keys(), key=lambda x:x)
            data_list = list()
            if list(aux.keys())[0] == 0:
                data_list.extend(simulateFailures(
                            aux[list(aux.keys())[0]],dist[0],dist[1]))
                aux.pop(0)
            for key in aux.keys():
                #Creamos una lista de nan y después le agregamos datos
                if (len(data_list) < aux[key]):
                    data_list.extend([1.0 for i in 
                                      range(abs(len(data_list)-aux[key]))])
                data_list.extend(simulateFailures(aux[key],dist[0],dist[1]))
            return(self.getIndex(data = data_list, type='nan',h=h))

        except:
            raise Exception("KEYERROR: colname don't found")

    def getIndex(self,data=None,colname:str ='data',type:str='data',h=None)->dict:
        """Return a dict of the initial position of a seccuens of type data

        Parameters
        ----------
        colname : str, optional
            Name of column to serch into, by default 'data'
        type : str, optional
            Type of data to find, by default 'data'
        """        
        if not (data is None):
            aux = [True if val != 0 or val is False else False for val in data]
        else:
            try:
                aux = ~(self.df[colname].isna())
            except Exception as e:
                print(e)        
    
        if type == 'nan':
            try:
                aux = ~(aux)
            except:
                aux = [ 0 if i == 1 else 1 for i in aux ]

        i = 0
        dic = dict()

        for index in range(len(aux)):
            if aux[index]:
                i += 1
            else:
                if i != 0:
                    dic[index-i] = i
                i = 0
        if i != 0:
            dic[index-i+1] = i

        if h == None:
            return(dic)
        else:
            #keys = [key for key in list(
             #   positions.keys()) if positions[key] == h]
            aux = dict()
            for key,val in dic.items():
                if val == h:
                    aux[key] = val
            return(aux)
            

    def addCol(self,colname:str,data:list=[]):
        """Add a new column to data strucutre

        Parameters
        ----------
        data : list
            Data input
        colname : str
            Name of data
        """        
        if len(data) == 0:
            data = [np.nan for i in range(self.df.shape[0])]
            self.df[colname] = data
        elif len(data) != self.df.shape[0]:
            if len(data) > self.df.shape[0]:
                data = data[:self.df.sahpe[0]]
            else:
                n = self.df.shape[0] - len(data)
                aux = ['*' for i in range(n)]
                data += aux
            
            self.df[colname] = data
        else:
            self.df[colname] = data
        
    
    def drop(self,colnames:list) -> list:
        """
        Elimina la columna bajo el nombre colname de la estrucutura de datos
            
        Inputs
        ------
        colname:list(str)
            Nombre de la columna que sera retirada
            
        Outputs
        -------
        list
            Datos retirados del objeto
        """
        try:
            out = self.df[colnames]
            self.df.drop(colnames,axis=1,inplace=True)
            return(out)
        except:
            raise Exception("KEY: Nombre de columna no establecido")
        

    def export(self,type:str='csv'):
        """[summary]

        Parameters
        ----------
        type : str, optional
            [description], by default 'csv'
        """
        if type == 'csv':
            self.df.to_csv('{}.csv'.format(self.name),index=False)
        elif type == 'xlsx':
            self.df.to_excel('{}.xlsx'.format(self.name),index=False)


            
##########
###Pruebas a objetos
#data = [2, 1, 2, np.nan, 4, 5, np.nan]
#date = ['2017-12-31','2017-12-31','2018-01-31',
#        '2018-02-27','2018-03-31','2019-12-31',
#        '2020-12-31']
#name = "Prueba"

#rd = RestoreData(data,date,name)
#print(rd.df.head(7))
#print(rd.getFakeIndex(colname='data'))
#print(rd.getIndex("data"))
#print(rd.getIndex(type='nan'))
#a = rd.df.groupby(pd.Grouper(key='date',freq='Y'),dropna=True).agg('count')
#print(a['date']-a['data'])
#print(a.columns())
#print(a)
#a,b = rd.counter('data','time')
#print(a,'\n',b)
#fig, ax = plt.subplots(2,3,sharex=True)
#fig.suptitle("Grafica prueba")
#rd.plot_dot(ax,color=1)
#plt.show()
#print(rd.df.head(6))
#rd.describe()



###Pruebas a funciones

#bateos = [5659,  5710, 5563, 5672, 5532, 5600, 5518, 5447, 5544, 5598,
   #       5585, 5436, 5549, 5612, 5513, 5579, 5502, 5509, 5421, 5559,
  #        5487, 5508, 5421, 5452, 5436, 5528, 5441, 5486, 5417, 5421]

#runs = [855, 875, 787, 730, 762, 718, 867, 721, 735, 615, 708, 644, 654, 735,
#        667, 713, 654, 704, 731, 743, 619, 625, 610, 645, 707, 641, 624, 570,
 #       593, 556]

#X = list(range(len(bateos)))
#y = lambda a,b,x: a + b*x
